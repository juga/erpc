use super::super::{config::Neo4jConfig, relay::Node};
use deadqueue::unlimited::Queue;
use erpc_scanner::work::{CompletedWork, CompletedWorkStatus};
use log::error;
use neo4rs::query;
use std::sync::Arc;
use std::time::Duration;
use tokio::time::sleep;

const RETRY_INTERVAL_ON_ERROR: u64 = 1;

pub struct Neo4jDbClient {
    /// The Neo4j connection
    graph: Arc<neo4rs::Graph>,

    /// A completed work queue
    completed_work_queue: Arc<Queue<CompletedWork>>,
}

impl Neo4jDbClient {
    /// Creates a new [Neo4jDbClient] from the given [Neo4jConfig]
    pub async fn new(neo4j_config: &Neo4jConfig) -> anyhow::Result<Self> {
        let uri = neo4j_config.uri.clone();
        let username = neo4j_config.username.clone();
        let password = neo4j_config.password.clone();

        let graph = Arc::new(neo4rs::Graph::new(uri, username, password).await?);
        let completed_work_queue = Arc::new(Queue::new());

        let neo4jdbclient = Self {
            graph,
            completed_work_queue,
        };

        neo4jdbclient.start_getting_completed_works();

        Ok(neo4jdbclient)
    }

    fn start_getting_completed_works(&self) {
        let completed_work_queue = self.completed_work_queue.clone();
        let neo4j_connection = self.graph.clone();
        tokio::task::spawn(async move {
            loop {
                let completed_work = completed_work_queue.pop().await;
                'error_recovery: loop {
                    let guard_relay = &completed_work.source_relay;
                    let exit_relay = &completed_work.destination_relay;
                    let timestamp = completed_work.timestamp;
                    let (status, message) = {
                        match completed_work.status {
                            CompletedWorkStatus::Success => (1, String::from("Success")),
                            CompletedWorkStatus::Failure(ref message) => (0, message.clone()),
                        }
                    };
                    let raw_query = format!(
                        "MATCH (guard_relay:Relay {{fingerprint: $guard_relay_fingerprint}})
                           MATCH (exit_relay:Relay {{fingerprint: $exit_relay_fingerprint}})
                           MERGE (guard_relay)-[rel:{}]->(exit_relay)
                           SET rel.message = $message, rel.timestamp = $timestamp ",
                        if status == 1 { "CIRCUIT_SUCCESS" } else { "CIRCUIT_FAILURE" }
                    );

                    let query = query(&raw_query)
                        .param("guard_relay_fingerprint", guard_relay.as_str())
                        .param("exit_relay_fingerprint", exit_relay.as_str())
                        .param("message", message)
                        .param("timestamp", timestamp.to_string());
                    if neo4j_connection.run(query).await.is_ok() {
                        break 'error_recovery;
                    }

                    sleep(Duration::from_secs(RETRY_INTERVAL_ON_ERROR)).await;
                }
            }
        });
    }

    /// Add an edge i.e circuit creation attempt into the database
    pub async fn add_completed_work(&self, completed_work: CompletedWork) {
        self.completed_work_queue.push(completed_work);
    }

    /// Add the indexing item of the relay i.e the fingerprint of the relay
    pub async fn add_node(&self, node: Arc<Node>) {
        let query = query("MERGE (:Relay {fingerprint:  $fingerprint})").param("fingerprint", node.fingerprint());
        if let Err(err) = self.graph.run(query).await {
            error!(
                "Coudln't add the relay with fingerprint {} in the neo4j database | Error : {err:?}",
                node.fingerprint(),
            );
        }
    }
}
