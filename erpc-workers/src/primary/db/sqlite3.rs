use super::super::config::Sqlite3Config;
use deadqueue::unlimited::Queue;
use erpc_scanner::work::{CompletedWork, CompletedWorkStatus};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::params;
use std::{sync::Arc, time::Duration};
use tokio::time::sleep;

const TABLE_NAME: &str = "circuits";
const RETRY_INTERVAL_ON_ERROR: u64 = 1;

pub struct Sqlite3DbClient {
    /// Connection pool to the sqlite3 database
    sqlite3_connection_pool: Arc<Pool<SqliteConnectionManager>>,

    /// A completed work queue
    completed_work_queue: Arc<Queue<CompletedWork>>,
}

impl Sqlite3DbClient {
    /// Creates a new [Sqlite3DbClient] from the given [Sqlite3Config]
    pub fn new(sqlite3_config: &Sqlite3Config) -> anyhow::Result<Self> {
        let sqlite3_connection_manager = SqliteConnectionManager::file(sqlite3_config.path.as_str());
        let sqlite3_connection_pool = Arc::new(Pool::new(sqlite3_connection_manager)?);
        let connection = sqlite3_connection_pool.get()?;
        let completed_work_queue = Arc::new(Queue::new());

        let query = format!(
            "CREATE TABLE IF NOT EXISTS {} (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                guard_relay TEXT,
                exit_relay TEXT,
                timestamp INTEGER,
                status INTEGER,
                message TEXT
            );",
            TABLE_NAME
        );
        connection.execute(&query, params![])?;

        let sqlite3dbclient = Self {
            sqlite3_connection_pool,
            completed_work_queue,
        };

        sqlite3dbclient.start_getting_completed_works();

        Ok(sqlite3dbclient)
    }

    /// Starts receiving [CompletedWork] from the queue, it will continuously try to add
    /// required item into the sqlite3 database until it gets inserted
    pub fn start_getting_completed_works(&self) {
        let completed_work_queue = self.completed_work_queue.clone();
        let sqlite3_connection_pool = self.sqlite3_connection_pool.clone();

        tokio::task::spawn(async move {
            loop {
                let completed_work = completed_work_queue.pop().await;
                'error_recovery: loop {
                    if let Ok(connection) = sqlite3_connection_pool.get() {
                        let guard_relay = &completed_work.source_relay;
                        let exit_relay = &completed_work.destination_relay;
                        let timestamp = &completed_work.timestamp;
                        let (status, message) = {
                            match completed_work.status {
                                CompletedWorkStatus::Success => (1, String::from("Success")),
                                CompletedWorkStatus::Failure(ref message) => (0, message.clone()),
                            }
                        };
                        let query = format!(
                            "INSERT INTO {} (guard_relay, exit_relay, timestamp, status, message) VALUES (?1, ?2, ?3, ?4, ?5)",
                            TABLE_NAME
                        );

                        if connection
                            .execute(query.as_str(), params![guard_relay, exit_relay, timestamp, status, message])
                            .is_ok()
                        {
                            break 'error_recovery;
                        }
                    }
                    sleep(Duration::from_secs(RETRY_INTERVAL_ON_ERROR)).await;
                }
            }
        });
    }

    /// Add the result of circuit creation attempt into the sqlite3 database i.e [CompletedWork]
    pub fn add_completed_work(&self, completed_work: CompletedWork) {
        self.completed_work_queue.push(completed_work);
    }
}
