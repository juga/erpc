//use serde::{Deserialize, Serialize};
//use serde_json::Value;
//use std::collections::HashMap;
//
///// Data Structure that maps to OnionPerf analysis JSON data (Version : 3.1)
//#[derive(Serialize, Deserialize, Debug, Clone)]
//pub struct OnionPerf {
//    data: HashMap<String, Data>,
//
//    #[serde(skip_deserializing)]
//    filters: Option<Value>,
//
//    //Document type
//    #[serde(rename = "type")]
//    _type: String,
//
//    //Document version
//    version: String,
//}
//
//#[derive(Serialize, Deserialize, Debug, Clone)]
//pub struct Data {
//    ///Public IP address of the measuring host
//    measurement_ip: String,
//
//    ///Measurement data obtained from client-side TGen logs
//    tgen: Value,
//
//    //Metadata obtained from client-side Tor controller logs
//    tor: Value,
//}
