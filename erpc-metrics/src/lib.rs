//mod model;
//
//use serde::{Deserialize, Serialize};
//use serde_json::Value;
//use std::collections::HashMap;
//
///// Metadata about the runners/hosts that produces onion perf data
//#[derive(Debug, Clone)]
//pub struct OnionPerfRunner {
//    /// Name of the host
//    host_name: String,
//
//    /// The URL of the OnionPerf data produced by the host
//    url: String,
//
//    // The data of the host
//    //data: Option<OnionPerfAnalysisData>,
//    /// A list of ciruits(with path) marked successful by OnionPerf
//    successful_circuits: Vec<Circuit>,
//
//    /// A list of ciruits(with no path) marked failed by OnionPerf
//    failed_circuits: Vec<Circuit>,
//}
//
//#[derive(Serialize, Deserialize, Debug, Clone)]
//pub struct Circuit {
//    //Circuit identifier, obtained from CIRC and CIRC_MINOR events
//    circuit_id: usize,
//
//    //Elapsed seconds until receiving and logging CIRC and CIRC_MINOR events
//    elapsed_seconds: Vec<Value>,
//
//    /// Final end time of the circuit, obtained from the log time of the last CIRC CLOSED or CIRC FAILED event, given in seconds since the epoch
//    unix_ts_start: f32,
//
//    /// Initial start time of the circuit, obtained from the log time of the CIRC LAUNCHED event, given in seconds since the epoch"
//    unix_ts_end: f32,
//
//    failure_reason_local: Option<String>,
//
//    ///Build time in seconds, computed as time elapsed between CIRC LAUNCHED and CIRC BUILT events
//    buildtime_seconds: Option<f32>,
//
//    ///Whether this circuit has been filtered out when applying filters in `onionperf filter`
//    ///TODO: Figure out what onionperf filter means
//    filtered_out: Option<bool>,
//    //Path information
//    //path: Option<Vec<RelayDetail>>,
//}
